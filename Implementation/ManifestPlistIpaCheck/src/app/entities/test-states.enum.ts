export enum TestStates
{
    NotStarted,
    Pending,
    Running,
    Skipped,
    Success,
    Warning,
    Error
}
