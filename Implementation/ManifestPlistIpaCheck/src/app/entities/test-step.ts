import { TestStates } from "./test-states.enum";

export class TestStep
{
    public Title: string;

    public State: TestStates = TestStates.NotStarted;

    public Description: string;

    public Urls: Array<string> = [];

    public Details: any = {};

    public TestMethod: (testStep: TestStep, meta: any) => Promise<boolean>;

    public get StateName(): string
    {
        return TestStates[this.State];
    }

    constructor(title: string, testMethod: (testStep: TestStep, meta: any) => Promise<boolean>)
    {
        this.Title = title;
        this.TestMethod = testMethod;
    }
}