import { TestStates } from "../entities/test-states.enum";
import { TestStep } from "../entities/test-step";
import { DownloadService } from "../services/download.service";

export class HtmlFileTests
{
    private _downloadService: DownloadService;

    constructor(downloadService: DownloadService)
    {
        this._downloadService = downloadService;
    }

    public GetTests(): Array<TestStep>
    {
        const list: Array<TestStep> = [];

        list.push(new TestStep("HTTPS Check", this.HttpsTest.bind(this)));
        list.push(new TestStep("HTML-File Download", this.HtmlDownloadTest.bind(this)));
        list.push(new TestStep("itms-services:// Link", this.ContainsItmsServicesTest.bind(this)));
        list.push(new TestStep("itms-services Target Check", this.ItmsServiceLinkTest.bind(this)));

        return list;
    }

    public HttpsTest(testStep: TestStep, meta: any): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            if (meta.HtmlUrl.startsWith("https://"))
            {
                testStep.State = TestStates.Success;
                testStep.Description = "HTML URL starts with https://";
                resolve(true);
            }
            else
            {
                testStep.State = TestStates.Error;
                testStep.Description = "HTML URL does not start with https://";
                resolve(false);
            }
        });
    }

    public HtmlDownloadTest(testStep: TestStep, meta: any): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._downloadService.GetFile(meta.HtmlUrl)
                .subscribe(response =>
                    {
                        try
                        {
                            meta.HtmlContent = response.text();
                            const contentType = response.headers.get("content-type");
                            if (contentType !== "text/html" && contentType != "application/xhtml+xml")
                            {
                                testStep.State = TestStates.Error;
                                testStep.Description = `The content type of the HTML file should be text/html but it was ${contentType}. Check your server configuration.`;
                                resolve(false);
                            }
                            else if (meta.HtmlContent)
                            {
                                testStep.State = TestStates.Success;
                                testStep.Description = `Download was successful. HTML file contains ${meta.HtmlContent.length} characters.`;
                                resolve(true);
                            }
                            else
                            {
                                testStep.State = TestStates.Error;
                                testStep.Description = `HTML file is empty.`;
                                resolve(false);
                            }
                        }
                        catch (error)
                        {
                            testStep.State = TestStates.Error;
                            testStep.Description = error;
                            reject(error);
                        }
                    },
                    errorResponse =>
                    {
                        // Check errorResponse.type -- CORS?
                        testStep.State = TestStates.Error;
                        testStep.Description = `${errorResponse} - It is possible that the download was blocked due to CORS policy. Check your browser console for more details, go to test-cors.org or use the CORS-Proxy.`;
                        testStep.Urls.push("https://test-cors.org/");
                        testStep.Urls.push("https://cors-anywhere.herokuapp.com/");
                        testStep.Urls.push("https://stackoverflow.com/questions/14467673/enable-cors-in-htaccess");
                        reject(errorResponse);
                    });
        });
    }

    public ContainsItmsServicesTest(testStep: TestStep, meta: any): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            try
            {
                const parser = new DOMParser();
                const parsedHtml = parser.parseFromString(meta.HtmlContent, "text/html");
                if (parsedHtml)
                {
                    const links = parsedHtml.getElementsByTagName("a");
                    const foundLinks: Array<string> = [];
                    for (let i: number = 0; i < links.length; i++)
                    {
                        const link = links.item(i);
                        if (link.href && link.href.startsWith("itms-services://"))
                        {
                            foundLinks.push(link.href);
                        }
                    }

                    if (foundLinks.length <= 0)
                    {
                        testStep.Description = `No itms-services:// links found in the html document.`;
                        testStep.State = TestStates.Error;
                        resolve(false);
                    }
                    else if (foundLinks.length === 1 || foundLinks.every((val, i, arr) => val === arr[0]))
                    {
                        meta.ItmsServiceUrl = foundLinks[0];
                        testStep.Description = `Found one itms-services:// link: ${foundLinks[0]}`;
                        testStep.State = TestStates.Success;
                        resolve(true);
                    }
                    else
                    {
                        meta.ItmsServiceUrl = foundLinks[0];
                        testStep.Description = `Found different itms-services:// links. I'm confused, which one should I test. I'm taking the first one: ${foundLinks[0]}`;
                        testStep.State = TestStates.Warning;
                        resolve(true);
                    }
                }
                else
                {
                    testStep.State = TestStates.Error;
                    testStep.Description = "Error parsing HTML file. It looks like there are syntax errors in the document. Use https://validator.w3.org/ to check for errors.";
                    testStep.Urls.push("https://validator.w3.org/");
                    resolve(false);
                }
            }
            catch (error)
            {
                testStep.State = TestStates.Error;
                testStep.Description = "Error parsing HTML file. It looks like there are syntax errors in the document. Use https://validator.w3.org/ to check for errors.";
                testStep.Urls.push("https://validator.w3.org/");
                reject(error);
            }
        });
    }

    public ItmsServiceLinkTest(testStep: TestStep, meta: any): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            if (meta.HtmlContent.indexOf("download-manifest&url=") >= 0)
            {
                testStep.State = TestStates.Error;
                testStep.Description = `All ampersands should be escaped. &url= should be &amp;url=. Your URL: ${meta.ItmsServiceUrl}`;
                resolve(false);
            }
            else if (meta.ItmsServiceUrl.startsWith("itms-services://?action=download-manifest&url="))
            {
                meta.PlistUrl = meta.ItmsServiceUrl.replace("itms-services://?action=download-manifest&url=", "").split("&")[0];
                if (!meta.PlistUrl.startsWith("https://"))
                {
                    testStep.State = TestStates.Error;
                    testStep.Description = `URL starts with itms-services://?action=download-manifest&amp;url= but the target URL does not use encryption (https://). Your target URL: ${meta.PlistUrl}`;
                    resolve(false);
                }
                else if (!meta.PlistUrl.endsWith(".plist"))
                {
                    testStep.State = TestStates.Error;
                    testStep.Description = `URL starts with itms-services://?action=download-manifest&amp;url= but the target URL does not point to a .plist file (extension should be lower case). Your target URL: ${meta.PlistUrl}`;
                    resolve(false);
                }
                else
                {
                    testStep.State = TestStates.Success;
                    testStep.Description = "URL starts with itms-services://?action=download-manifest&amp;url= and the target URL points to a .plist file protected by TLS.";
                    resolve(true);
                }
            }
            else
            {
                testStep.State = TestStates.Error;
                testStep.Description = `Your link does not look correct. It should look like itms-services://?action=download-manifest&amp;url=https://link-to-your-manifest.plist but yours is ${meta.ItmsServiceUrl}`;
                resolve(false);
            }
        });
    }
}
