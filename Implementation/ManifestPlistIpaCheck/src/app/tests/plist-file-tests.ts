import { rootRoute } from "@angular/router/src/router_module";
import { TestStates } from "../entities/test-states.enum";
import { TestStep } from "../entities/test-step";
import { DownloadService } from "../services/download.service";

export class PlistFileTests
{
    private _downloadService: DownloadService;

    constructor(downloadService: DownloadService)
    {
        this._downloadService = downloadService;
    }

    private GetChildNode(node: Node): Node
    {
        let result: Node = node.firstChild;
        while (result && result.nodeName && result.nodeName.toUpperCase() === "#TEXT")
        {
            result = result.nextSibling;
        }
        return result;
    }

    private GetNextNode(node: Node): Node
    {
        let result: Node = node.nextSibling;
        while (result && result.nodeName && result.nodeName.toUpperCase() === "#TEXT")
        {
            result = result.nextSibling;
        }
        return result;
    }

    private GetPlistValue(node: Node, key: string): string
    {
        if (node.nodeName.toUpperCase() === "KEY" && node.textContent === key)
        {
            const valueNode = this.GetNextNode(node);
            if (valueNode.nodeName.toUpperCase() === "STRING")
            {
                return valueNode.textContent;
            }
        }

        for (let i = 0; i < node.childNodes.length; i++)
        {
            const value = this.GetPlistValue(node.childNodes.item(i), key);
            if (value)
            {
                return value;
            }
        }

        return null;
    }

    public GetTests(): Array<TestStep>
    {
        const list: Array<TestStep> = [];

        list.push(new TestStep("P-list File Download", this.PlistDownloadTest.bind(this)));
        list.push(new TestStep("P-list File Structure", this.PlistStructureTest.bind(this)));

        return list;
    }

    public PlistDownloadTest(testStep: TestStep, meta: any): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._downloadService.GetFile(meta.PlistUrl)
                .subscribe(response =>
                    {
                        try
                        {
                            meta.PlistContent = response.text();
                            const contentType = response.headers.get("content-type");
                            if (contentType !== "application/xml")
                            {
                                testStep.State = TestStates.Error;
                                testStep.Description = `The content type of the p-list file should be application/xml but it was ${contentType}. Check your server configuration.`;
                                resolve(false);
                            }
                            else if (meta.PlistContent)
                            {
                                testStep.State = TestStates.Success;
                                testStep.Description = `Download was successful. P-list file contains ${meta.PlistContent.length} characters.`;
                                resolve(true);
                            }
                            else
                            {
                                testStep.State = TestStates.Error;
                                testStep.Description = `P-list file is empty.`;
                                resolve(false);
                            }
                        }
                        catch (error)
                        {
                            testStep.State = TestStates.Error;
                            testStep.Description = error;
                            reject(error);
                        }
                    },
                    error =>
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = error;
                        reject(error);
                    });
        });
    }

    public PlistStructureTest(testStep: TestStep, meta: any): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            try
            {
                const parser = new DOMParser();
                const parsedXml = parser.parseFromString(meta.PlistContent, "text/html");
                if (parsedXml)
                {
                    const rootNode = this.GetChildNode(parsedXml.body);
                    if (rootNode.nodeName.toUpperCase() !== "PLIST")
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The root node of the p-list file is not <plist></plist>. It is ${rootNode.nodeName}.`;
                        resolve(false);
                        return;
                    }

                    const dictNode = this.GetChildNode(rootNode);
                    if (dictNode.nodeName.toUpperCase() !== "DICT")
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The first node in <plist> is not <dict></dict>. It is ${dictNode.nodeName}.`;
                        resolve(false);
                        return;
                    }

                    const keyNode = this.GetChildNode(dictNode);
                    if (keyNode.nodeName.toUpperCase() !== "KEY")
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The first node in <dict> is not <key></key>. It is ${keyNode.nodeName}.`;
                        resolve(false);
                        return;
                    }

                    const mainArrayNode = this.GetNextNode(keyNode);
                    if (mainArrayNode.nodeName.toUpperCase() !== "ARRAY")
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The node after <key> is not <array></array>. It is ${mainArrayNode.nodeName}.`;
                        resolve(false);
                        return;
                    }

                    const mainDictNode = this.GetChildNode(mainArrayNode);
                    if (mainDictNode.nodeName.toUpperCase() !== "DICT")
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The first node in <array> is not <dict></dict>. It is ${mainDictNode.nodeName}.`;
                        resolve(false);
                        return;
                    }


                    // bundle-version
                    meta.BundleVersion = this.GetPlistValue(rootNode, "bundle-version") || "";
                    let parts = meta.BundleVersion.split(".");
                    if (parts.length !== 3 || meta.BundleVersion === "0.0.0")
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The bundle-version has to be in the format MAJOR.MINOR.PATCH and has to be higher than 0.0.0 (more information at https://semver.org/). Your bundle-version: ${meta.BundleVersion || "<EMPTY>"}`;
                        testStep.Urls.push("https://semver.org/");
                        resolve(false);
                        return;
                    }

                    // bundle-identifier
                    meta.BundleIdentifier = this.GetPlistValue(rootNode, "bundle-identifier") || "";
                    parts = meta.BundleIdentifier.split(".");
                    if (parts.length <= 1)
                    {
                        testStep.State = TestStates.Error;
                        testStep.Description = `The bundle-identifier is a reverse-DNS notation for your project and cannot be empty. Your bundle-identifier: ${meta.BundleIdentifier || "<EMPTY>"}`;
                        resolve(false);
                        return;
                    }

                    testStep.Details["bundle-identifier"] = meta.BundleIdentifier;
                    testStep.Details["bundle-version"] = meta.BundleVersion;

                    testStep.State = TestStates.Warning;
                    testStep.Description = "P-list file looks good so far. TODO: More tests.";
                    resolve(true);
                }
                else
                {
                    testStep.State = TestStates.Error;
                    testStep.Description = "Error parsing P-list file. It looks like there are syntax errors in the document.";
                    resolve(false);
                }
            }
            catch (error)
            {
                testStep.State = TestStates.Error;
                testStep.Description = "Error parsing P-list file. It looks like there are syntax errors in the document.";
                reject(error);
            }
        });
    }
}
