import { Component } from "@angular/core";
import { TestStep } from "../entities/test-step";
import { TestRunnerService } from "../services/test-runner.service";
import { DownloadService } from "./../services/download.service";
import { LoggingService } from "./../services/logging.service";

@Component({
    selector: "app-home",
    templateUrl: "home.page.html",
    styleUrls: ["home.page.scss"]
})
export class HomePage
{
    private _corsProxy: boolean = false;

    public Logs: Array<any>;
    public TestSteps: Array<TestStep>;

    public HtmlUrl: string = "";

    public get CorsProxy(): boolean
    {
        return this._corsProxy;
    }
    public set CorsProxy(value: boolean)
    {
        this._corsProxy = value;
        this._downloadService.UseCorsProxy = value;
    }

    constructor(private _loggingService: LoggingService, private _testRunner: TestRunnerService, private _downloadService: DownloadService)
    {
        this._loggingService.Intercept();
        this.Logs = this._loggingService.Messages;

        this.TestSteps = this._testRunner.TestSteps;
    }

    public StartTest()
    {
        this._testRunner.RunTests(this.HtmlUrl);
    }

    public Log()
    {
        console.log(this._loggingService.Messages);
    }
}
