import { Pipe, PipeTransform } from "@angular/core";
import { TestStates } from "../entities/test-states.enum";

@Pipe({
  name: "stateToIcon"
})
export class StateToIconPipe implements PipeTransform
{
    private _mapping: object = {
        NotStarted : "radio-button-off",
        Pending : "arrow-dropright-circle",
        Running : "play",
        Skipped : "close",
        Success : "checkmark-circle",
        Warning : "alert",
        Error : "warning"
        };

    public transform(value: any, args?: any): any
    {
        return this._mapping[TestStates[value]];
    }
}
