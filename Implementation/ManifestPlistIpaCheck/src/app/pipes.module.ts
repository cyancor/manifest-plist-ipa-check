import { NgModule } from "@angular/core";
import { StateToIconPipe } from "./pipes/state-to-icon.pipe";

@NgModule({
    declarations: [ StateToIconPipe ],
    imports: [],
    exports: [ StateToIconPipe ]
})
export class PipesModule {}