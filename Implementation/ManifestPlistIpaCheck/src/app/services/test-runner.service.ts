import { Injectable } from "@angular/core";
import { TestStates } from "../entities/test-states.enum";
import { TestStep } from "../entities/test-step";
import { PlistFileTests } from "../tests/plist-file-tests";
import { HtmlFileTests } from "./../tests/html-file-tests";
import { DownloadService } from "./download.service";
import { LoggingService } from "./logging.service";

@Injectable({
    providedIn: "root"
})
export class TestRunnerService
{
    private _testMeta: any = {};

    public TestSteps: Array<TestStep> = [];

    constructor(private _downloadService: DownloadService, private _loggingService: LoggingService)
    {
        this.PrepareTests();
    }

    private StopTestRun(lastIndex: number)
    {
        for (let index = lastIndex + 1; index < this.TestSteps.length; index++)
        {
            this.TestSteps[index].State = TestStates.Skipped;
            this.TestSteps[index].Description = "Skipped";
        }
    }

    private RunTest(testIndex: number)
    {
        const testStep = this.TestSteps[testIndex];
        if (!testStep)
        {
            this.StopTestRun(testIndex);
            return;
        }

        testStep.State = TestStates.Running;
        testStep.TestMethod(testStep, this._testMeta)
            .then(result => {
                if (result)
                {
                    console.log(`Test "${testStep.Title}" ran successfully.`);
                    this.RunTest(testIndex + 1);
                }
                else
                {
                    console.log(`Test "${testStep.Title}" returned a negative result.`);
                    this.StopTestRun(testIndex);
                }
             })
             .catch(error => {
                console.log(`Test "${testStep.Title}" ran with errors.`);
                console.log(error);
                this.StopTestRun(testIndex);
                });
    }

    private PrepareTests()
    {
        this.TestSteps.splice(0, this.TestSteps.length);

        const htmlTests = new HtmlFileTests(this._downloadService);
        this.TestSteps.push(... htmlTests.GetTests());
        const plistTests = new PlistFileTests(this._downloadService);
        this.TestSteps.push(... plistTests.GetTests());

        this._testMeta = {};
    }

    public RunTests(htmlUrl: string)
    {
        this._loggingService.Clear();

        this.PrepareTests();
        this._testMeta.HtmlUrl = htmlUrl;

        this.RunTest(0);
    }
}
