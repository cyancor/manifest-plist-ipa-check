import { Injectable } from "@angular/core";
import { Http, RequestOptionsArgs, Response } from "@angular/http";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class DownloadService
{
    public UseCorsProxy: boolean;

    constructor(private _http: Http)
    {
    }

    public GetFile(url: string, preventCache: boolean = true): Observable<Response>
    {
        let finalUrl = url;
        if (preventCache)
        {
            finalUrl = finalUrl + (url.indexOf("?") >= 0 ? "&t=" + Date.now() : "?t=" + Date.now());
        }

        if (this.UseCorsProxy)
        {
            finalUrl = "https://cors-anywhere.herokuapp.com/" + finalUrl;
        }

        return this._http.get(finalUrl);
    }
}
