import { TestBed } from '@angular/core/testing';

import { TestRunnerService } from './test-runner.service';

describe('TestRunnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestRunnerService = TestBed.get(TestRunnerService);
    expect(service).toBeTruthy();
  });
});
