import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class LoggingService
{
    public _maxMessages: number = 250;

    public Messages: Array<any> = [];

    private PrepareMessages(...messages: Array<any>)
    {
        messages.map(message =>
        {
            if (typeof message === "object")
            {
                try
                {
                    return JSON.stringify(message);
                }
                catch (error)
                {
                    // Mute all exceptions to prevent recursive logs
                }
            }
            return message;
        });
    }

    private log(...messages: Array<any>)
    {
        this.info(messages);
    }

    private info(...messages: Array<any>)
    {
        this.PrepareMessages(messages);
        this.Messages.splice(0, 0, [new Date().toISOString(), "NFO", ...messages]);
        this.Messages.length = Math.min(this.Messages.length, this._maxMessages);
    }

    private debug(...messages: Array<any>)
    {
        this.PrepareMessages(messages);
        this.Messages.splice(0, 0, [new Date().toISOString(), "DBG", ...messages]);
        this.Messages.length = Math.min(this.Messages.length, this._maxMessages);
    }

    private warn(...messages: Array<any>)
    {
        this.PrepareMessages(messages);
        this.Messages.splice(0, 0, [new Date().toISOString(), "WRN", ...messages]);
        this.Messages.length = Math.min(this.Messages.length, this._maxMessages);
    }

    private error(...messages: Array<any>)
    {
        this.PrepareMessages(messages);
        this.Messages.splice(0, 0, [new Date().toISOString(), "ERR", ...messages]);
        this.Messages.length = Math.min(this.Messages.length, this._maxMessages);
    }

    public Intercept()
    {
        const interceptFunction = (method: string) =>
        {
            const original = console[method];
            if (!original)
            {
                return;
            }

            const self = this;
            console[method] = function()
            {
                self[method].apply(self, arguments);
                if (original.apply)
                {
                    original.apply(console, arguments);
                }
                else
                {
                    const message = Array.prototype.slice.apply(arguments).join(" ");
                    original(message);
                }
            };
        };
        ["log", "info", "debug", "warn", "error"].forEach(method =>
        {
            interceptFunction(method);
        });
    }

    public Clear()
    {
        this.Messages.splice(0, this.Messages.length);
    }
}
